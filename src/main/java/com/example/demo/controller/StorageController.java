package com.example.demo.controller;

import com.example.demo.model.Source;
import com.example.demo.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.nio.ByteBuffer;
import java.util.List;

@RestController
@RequestMapping("/v1")
public class StorageController {

    @Autowired
    private StorageService storageService;

    @PostMapping("/upload")
    public Mono<ResponseEntity<com.example.demo.model.File>> upload(@RequestParam(value = "fileName") MultipartFile file, @RequestParam(value = "src") String folder) {
        return storageService.upload(file, folder).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping("/download")
    public Mono<ResponseEntity<ByteBuffer>> download(@RequestParam(value = "fileName") String fileName, @RequestParam(value = "src") String source) {
        return storageService.download(fileName.trim(), source.trim()).map( e-> {
           return ResponseEntity.ok().body(e);
        });
    }

    @PostMapping("/create_source")
    public Mono<ResponseEntity<Source>> createFolder(@RequestParam(value = "src") String source) {
        return storageService.createSource(source).map(e -> {
            return ResponseEntity.ok().body(e);
        });
    }

    @GetMapping("/view")
    public Mono<ResponseEntity<com.example.demo.model.File>> view(@RequestParam(value = "fileName") String fileName, @RequestParam(value = "src") String source) {
        return storageService.view(fileName.trim(), source.trim()).map(s -> {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(s);
        });
    }

    @GetMapping("/list")
    public Mono<ResponseEntity<List<com.example.demo.model.File>>> list(){
        return storageService.listFiles().map(e -> {
            return ResponseEntity.status(HttpStatus.OK).body(e);
        });
    }

    @PutMapping("/copy")
    public Mono<ResponseEntity<com.example.demo.model.File>> copy(@RequestParam(value = "fileName") String fileName, @RequestParam(value = "src") String source, @RequestParam(value = "dest") String destination) {
        return storageService.copy(fileName, source, destination).map(e -> {
            return ResponseEntity.ok().body(e);
        });
    }

    @PutMapping("/rename")
    public Mono<ResponseEntity<com.example.demo.model.File>> rename(@RequestParam(value = "fileName") String fileName, @RequestParam(value = "src") String source, @RequestParam(value = "newName") String newName) {
        return storageService.rename(fileName, source, newName).map(e -> {
            return ResponseEntity.ok().body(e);
        });
    }

    @PutMapping("/move")
    public Mono<ResponseEntity<com.example.demo.model.File>> move(@RequestParam(value = "fileName") String fileName, @RequestParam(value = "src") String source, @RequestParam(value = "dest") String destination) {
        return storageService.move(fileName, source, destination).map( e -> {
           return ResponseEntity.ok().body(e);
        });
    }

    @DeleteMapping("/delete")
    public Mono<ResponseEntity<String>> deleteFile(@RequestParam(value = "fileName") String fileName, @RequestParam(value = "src") String source) {
        return storageService.deleteFile(fileName.trim(), source.trim()).map(e -> {
            return ResponseEntity.ok().body(e);
        });
    }
}
