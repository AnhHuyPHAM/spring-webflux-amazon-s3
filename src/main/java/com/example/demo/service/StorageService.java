package com.example.demo.service;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.example.demo.model.File;
import com.example.demo.model.Source;
import com.example.demo.utils.AmazonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

@Service
@Slf4j
public class StorageService {


    @Autowired
    private AmazonUtils utils;

    public Mono<File> upload(MultipartFile file, String folderName) {
        java.io.File fileObj = convertMultipartToFile(file);
        String key = "";
        try {
            while (true) {
                key = folderName.isEmpty() ? System.currentTimeMillis() + "_" + file.getOriginalFilename() : folderName + "/" +  System.currentTimeMillis() + "_" + file.getOriginalFilename();
                if (!utils.checkObjectExist(key))
                    break;
            }
            utils.putObject(key, fileObj);
        } catch (AmazonS3Exception e ) {
            log.error("Exception", e);
            throw new RuntimeException("File " + key + " cannot be uploaded...", e);
        }
        return Mono.just(new com.example.demo.model.File(key, key.substring(0, key.lastIndexOf("/")+1), utils.getPresignedURL(key)));
    }

    public Mono<ByteBuffer> download(String fileName, String folder){
        try {
            S3Object s3Object = folder.isEmpty() ? utils.getObject( fileName) : utils.getObject(folder + "/" + fileName);
            S3ObjectInputStream inputStream = s3Object.getObjectContent();
            byte[] content = IOUtils.toByteArray(inputStream);
            return Mono.just(ByteBuffer.wrap(content));
        }
        catch (Exception e ) {
            log.error("Exception", e);
            throw new RuntimeException(e.getMessage(),e);
        }
    }

    public Mono<Source> createSource(String folder) {
        return Mono.just(utils.createFolder(folder));
    }

    public Mono<com.example.demo.model.File> view(String fileName, String folder) {
        try{
            String key = folder.isEmpty() ? fileName : folder + "/" + fileName;
            if (utils.checkObjectExist(key))
                return Mono.just(new com.example.demo.model.File(fileName,"", utils.getPresignedURL(key)));
            else {
                throw new RuntimeException("File not found!");
            }
        }
        catch (Exception e ) {
            log.error("Exception", e);
            throw new RuntimeException(e.getMessage(),e);
        }
    }

    public Mono<List<File>> listFiles() {
        return Mono.just(utils.listObjectsInBucket());
    }

    public Mono<com.example.demo.model.File> copy(String fileName, String source, String dest) {
        return Mono.just(utils.copyObject(fileName, source, dest));
    }

    public Mono<com.example.demo.model.File> rename(String fileName, String source, String newName) {
        return Mono.just(utils.renameObject(source, fileName, newName));
    }

    public Mono<com.example.demo.model.File> move(String fileName, String source, String dest) {
        return Mono.just(utils.moveObject(fileName, source, dest));
    }

    public Mono<String> deleteFile(String fileName, String folder) {
        try {
            String key = folder.isEmpty() ? fileName : folder + "/" + fileName;
            utils.deleteObject( key);
        } catch (Exception e) {
            log.error("Exception", e);
            throw new RuntimeException(e.getMessage(),e);
        }
        return Mono.just(fileName + " removed ...");
    }

    private java.io.File convertMultipartToFile(MultipartFile multipartFile) {
        java.io.File convertedFile = new java.io.File(multipartFile.getOriginalFilename());
        try (FileOutputStream fos = new FileOutputStream(convertedFile)){
            fos.write(multipartFile.getBytes());
        } catch (IOException e) {
            log.error("Error when convert ",e);
            throw new RuntimeException(e.getMessage(),e);
        }
        return convertedFile;

    }

}
