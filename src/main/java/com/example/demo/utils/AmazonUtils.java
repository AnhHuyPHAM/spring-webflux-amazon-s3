package com.example.demo.utils;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.example.demo.model.Source;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Component
public class AmazonUtils {

    @Value("${application.aws.bucket.name}")
    private String bucketName;

    @Autowired
    private AmazonS3 s3Client;

    public S3Object getObject(String key) throws AmazonS3Exception {
        return s3Client.getObject(bucketName, key);
    }

    public boolean checkObjectExist(String key) throws AmazonS3Exception{
        return s3Client.doesObjectExist(bucketName, key);
    }

    public PutObjectResult putObject(String key, File file) throws AmazonS3Exception{
        return s3Client.putObject(new PutObjectRequest(bucketName, key, file).withCannedAcl(CannedAccessControlList.AuthenticatedRead));
    }

    public Source createFolder(String key) throws AmazonS3Exception {
        InputStream inputStream = new ByteArrayInputStream(new byte[0]);
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);
        s3Client.putObject(new PutObjectRequest(bucketName, key + "/", inputStream, metadata));
        return new Source(key, getPresignedURL(key));
    }

    public boolean deleteObject(String key) throws AmazonS3Exception{
        s3Client.deleteObject(new DeleteObjectRequest(bucketName, key));
        return !checkObjectExist(key);
    }

    public URL getPresignedURL(String key) throws AmazonS3Exception{
        java.util.Date expiration = new java.util.Date();
        long expTime = expiration.getTime();
        expTime += 1000*60;
        expiration.setTime(expTime);
        System.out.println(expiration);
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, key)
                .withMethod(HttpMethod.GET)
                .withExpiration(expiration);
        return s3Client.generatePresignedUrl(generatePresignedUrlRequest);
    }

    public List<com.example.demo.model.File> listObjectsInBucket() throws AmazonS3Exception{
        ObjectListing objectListing = s3Client.listObjects(bucketName);
        List<com.example.demo.model.File> result = new ArrayList<>();
        objectListing.getObjectSummaries().forEach(e -> {
            if(e.getSize()>0){
                String path = e.getKey().contains("/") ? e.getKey().substring(0, e.getKey().indexOf("/")+1) : "";
                result.add(new com.example.demo.model.File(e.getKey().substring(e.getKey().lastIndexOf("/")+1), path, getPresignedURL(e.getKey())));
            }
        });
        return result;
    }

    public com.example.demo.model.File copyObject(String fileName, String source, String destination) throws AmazonS3Exception{
        String oldKey = source.isEmpty() ? fileName : source + "/" + fileName;
        String newKey = destination.isEmpty() ? fileName : destination + "/" + fileName;
        if (checkObjectExist(oldKey) & checkObjectExist(newKey)) {
            s3Client.copyObject(new CopyObjectRequest(bucketName, oldKey, bucketName, newKey));
            com.example.demo.model.File result = new com.example.demo.model.File(fileName, destination, getPresignedURL(newKey));
            return result;
        }
        throw new RuntimeException("File not found!");
    }

    public com.example.demo.model.File moveObject(String fileName, String source, String destination) throws AmazonS3Exception{
        String oldKey = source.isEmpty() ? fileName : source + "/" + fileName;
        String newKey = destination.isEmpty() ? fileName : destination + "/" + fileName;
        if (checkObjectExist(oldKey) & checkObjectExist(newKey)) {
            s3Client.copyObject(new CopyObjectRequest(bucketName, oldKey, bucketName, newKey));
            com.example.demo.model.File result = new com.example.demo.model.File(fileName, destination, getPresignedURL(newKey));
            deleteObject(source);
            return result;
        }
        throw new RuntimeException("File not found!");
    }

    public com.example.demo.model.File renameObject(String source, String oldName, String newName){
        String oldKey = source.isEmpty() ? oldName : source + "/" + oldName;
        String newKey = source.isEmpty() ? newName : source + "/" + newName;
        if (checkObjectExist(source)) {
            s3Client.copyObject(new CopyObjectRequest(bucketName, oldKey, bucketName, newKey));
            deleteObject(source);
            com.example.demo.model.File result = new com.example.demo.model.File(newName, source, getPresignedURL(newKey));
            return result;
        }
        throw new RuntimeException("File not found!");
    }

    public com.example.demo.model.File convertS3ObjectToFile(S3Object object) {
        String key = object.getKey();
        return new com.example.demo.model.File(key.substring(key.indexOf("/") + 1), key.substring(0, key.indexOf("/") + 1), getPresignedURL(key));
    }
}
